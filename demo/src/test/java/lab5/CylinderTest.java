package lab5;
import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
    @Test
    public void testConstructor() {
        Cylinder newCylinder = new Cylinder(2,3);
        assertEquals(2, newCylinder.getRadius(), 0.0001);
        assertEquals(3, newCylinder.getHeight(), 0.0001);
    }
    @Test
    public void testGetVolume() {
        Cylinder newCylinder = new Cylinder(2,3);
        assertEquals(37.6991, Math.PI*Math.pow(newCylinder.getRadius(), 2)*newCylinder.getHeight(), 0.0001);
    }
    @Test
    public void testGetSurfaceArea() {
        Cylinder newCylinder = new Cylinder(2,3);
        assertEquals(62.8318, 2*Math.PI*Math.pow(newCylinder.getRadius(), 2)+(2*Math.PI*newCylinder.getRadius())*newCylinder.getHeight(), 0.0001);
    }
}