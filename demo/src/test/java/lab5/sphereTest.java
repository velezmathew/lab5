// James Capistran Beede
package lab5;
import static org.junit.Assert.*;
import org.junit.Test;
import java.lang.Math.*;
public class sphereTest {
    @Test
    public void testSphereConstructor(){
        Sphere t = new Sphere(3);
        assertEquals(3,t.getRadius()); 
    }
    @Test
    public void surfaceAreaTest() {
        Shape3d s = new Sphere(3);
        assertEquals(4 * Math.PI * Math.pow(3,2),s.getSurfaceArea(),0.001);
    }

    @Test
    public void volumeTest() {
        Shape3d s = new Sphere(2);
        assertEquals(Math.PI * Math.pow(2,3), s.getVolume(),0.001);
    }
}
