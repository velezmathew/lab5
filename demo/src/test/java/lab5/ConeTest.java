package lab5;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for Cone objetcs
 */
public class ConeTest {
    /**Test cone constructor */
    @Test
    public void testConeConstructor(){
        Cone b = new Cone(12, 4);
    }

    @Test
    public void testgetVolume(){
        Cone b = new Cone(12, 4);
        assertEquals(201.0619229, b.getVolume(), 0.0001);
    }

    @Test
    public void testgetSurfaceArea(){
        Cone b = new Cone(12, 4);
        assertEquals(209.2188947101, b.getSurfaceArea(), 0.0001);
    }

    @Test
    public void testGetHeight(){
        Cone b = new Cone(12, 4);
        assertEquals(12, b.getHeight(), 0.0001);

    }

    @Test
    public void testGetRadius(){
        Cone b = new Cone(12, 4);
        assertEquals(4, b.getRadius(), 0.0001);
    }

}
