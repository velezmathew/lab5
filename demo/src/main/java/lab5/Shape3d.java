package lab5;

interface Shape3d {
    public double getVolume();
    public double getSurfaceArea();
}