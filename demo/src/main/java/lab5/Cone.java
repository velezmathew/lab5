package lab5;

public class Cone {
    protected double radius;
    protected double height;

    public Cone(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    public double getVolume() {
        double v = Math.PI * this.radius * this.radius * this.height / 3;
        return v;
    }
    public double getSurfaceArea() {
        double sa = Math.PI * this.radius * (this.radius + Math.sqrt(this.height*this.height + this.radius*this.radius));
        return sa;
    }

    public double getHeight() {
        return this.height;
    }

    public double getRadius() {
        return this.radius;
    }


}
