package lab5;
import java.lang.Math.*;
public class Sphere implements Shape3d {
    private int radius;

    public Sphere(int radius) {
        this.radius = radius;
    }
    public int getRadius(){
        return this.radius;
    }
    /** 
     * Gets the volume of the shape
     * @return volume of the shape (double)
     */
    @Override
    public double getVolume() {
            return Math.PI * Math.pow(this.radius, 3);
    }


    /**
     * Gets the surface area of the object
     * @return surface area (double)
     */
    @Override
    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(this.radius,2);
    }
}
