package lab5;

public class Cylinder implements Shape3d{
    private double radius;
    private double height;
    /**
     * getVolume is pi * radius^2 * height
     * 
     */
        public Cylinder(double newRadius, double newHeight){
        this.radius = newRadius;
        this.height = newHeight;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getHeight(){
        return this.height;
    }

    public double getVolume(){
        return Math.PI*Math.pow(this.radius, 2)*this.height;
    }
    public double getSurfaceArea(){
        return 2*Math.PI*Math.pow(this.radius, 2)+(2*Math.PI*this.radius)*this.height;
    }
}
